alias sshl="ssh-add -l"
alias sshid="ssh -o IdentitiesOnly=yes"

alias startconky="conky -c ~/.config/conky/conky.conf &"
alias bat="batcat"
alias steam='GDK_SCALE=2 steam'

if [ -f "$HOME/.local/bin/aws" ] || [ -f /usr/local/bin/aws ]; then
    alias awsid="aws sts get-caller-identity"
    alias envaws="export AWS_SDK_LOAD_CONFIG=1"
    alias aws_sso_infra="aws sso login --profile infra"
fi

alias awsprofile='echo $AWS_PROFILE'

if [ -f /usr/local/bin/kubectl ] || [ -f /snap/bin/kubectl ]; then
    alias kpods='kubectl get pods'
    alias klogs='kubectl logs'
    alias kexec='kubectl exec -it'
fi

if [ -f /usr/bin/docker ]; then
    alias dockerdang='docker rmi $(docker images -f dangling=true -q --no-trunc)'
    alias dockerrm='docker rm $(docker ps -a -q)'
fi

if [ -f /usr/local/bin/sops ]; then
    alias sops-local='sops $(echo $kmsdev) $(echo $pgpdev) .local.env'
fi

if [ -f /usr/bin/terraform ] || [ -f $HOME/.local/bin/terraform ]; then
  alias tfp='terraform plan --var-file=tfvars/$(terraform workspace show).tfvars'
  alias tfa='terraform apply --var-file=tfvars/$(terraform workspace show).tfvars'
  alias tfd='terraform destroy --var-file=tfvars/$(terraform workspace show).tfvars'
  alias tfbeta='terraform workspace select beta'
  alias tfprod='terraform workspace select production'
  alias tfw='terraform workspace select '

  alias tffmt='terraform fmt'
fi
