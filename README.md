# Dotfiles

## Prerequisites

* [Stow](https://www.gnu.org/software/stow/)

## Usage

#### 1. Clone repo
`git clone https://bitbucket.org/ngstigator/dotfiles.git`

#### 2. Deploy config file(s)

Example: Bash

* `cd dotfiles`
* `stow bash`

This will create symlink to `.bashrc` and `.bash_aliases`.