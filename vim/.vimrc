set nocompatible

set encoding=utf-8
set fileencoding=utf-8

set backspace=indent,eol,start
" set expandtab
set tabstop=2
set softtabstop=2
set shiftwidth=2
set expandtab
set autoindent

colorscheme desert
set cursorline
" set cursorcolumn
hi CursorLine cterm=NONE ctermbg=237_Grey23
set showcmd

filetype on
filetype plugin on
filetype indent on

set wildmenu
set lazyredraw
set showmatch

set incsearch
set hlsearch!
" HOTKEY <Space> reset search highlighting
nnoremap <Space> :set hlsearch!<CR>

set ignorecase
set smartcase

" HOTKEY <f5> format json
nnoremap <f5> :%!python -m json.tool<CR>

" set relativenumber
